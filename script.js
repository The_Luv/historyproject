let itemNavs=document.getElementsByClassName("itemNav");
let color;
for (let i=0; i<itemNavs.length; i++) {
    if (itemNavs[i].classList.contains("active")) {
        color=getComputedStyle(document.body).getPropertyValue('--color'+i);
        document.getElementById("navigation").style.backgroundColor=color;
        document.getElementById("hamburger-wrapper").style.background=color;
        break;
    }
}
document.getElementById("info-box").style.backgroundColor=color;

function hamburger() {
    let hamburgerNav = document.getElementById("hamburger-nav");
    hamburgerNav.style.visibility='visible';
    hamburgerNav.style.bottom='0px';
    for (let i=0; i<itemNavs.length; i++) {
        itemNavs[i].style.visibility='visible';
    }
    document.body.classList.add("no-scroll");
}