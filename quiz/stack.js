let cards=document.getElementsByClassName("card");
let counter=0;
let correctCounter=0;
let answers=[2, 1, 0, 3, 2, 0, 3];
let freeze=false;
let gotWrong=false;
const answerPossibilities=[['the british', 'the persians', 'andean communities', 'tribes of the pacific'], 
['the high percentage of alcohol was seen as dangerous', 'absinthe can cause lifelong side effects', 'the wine farmers convinced the government to produce wine instead of absinthe to make money', 'the germans produced a lot of absinthe and they got much money from selling it'],
['morpheus is the greek god of sleep', 'a town in central france were it was invented', 'from the latin name of the opium poppy it is taken from, "morpheus somniferum"', 'it’s the name of the doctor that invented it, Henry Morphine'],
['People were encouraged not to drink as an act of solidarity for the soldiers', 'It has bad effects on the health of people', 'The war leaders agreed on a strict drinking policy in order to save money', 'it was a waste of the precious alcohol reserved for soldiers'],
['Russia', 'Italy', 'USA', 'Austria-Hungary'],
['canadian soldiers brought cocaine to britain and sold it', 'they feared that people would get addicted to it', 'it has been proven useless as a medicine and is expensive', 'the production of cocaine financed opposing nations'],
['it is quite expensive and difficult to produce', 'there was a shortage of opium poppys at the beginning of the 20th century', 'It is sensitive to temperatures and therefore difficult to store at the frontline', 'it is very addictive']];


function getActiveCard() {
    if (document.getElementsByClassName("active")[0]);
    return document.getElementsByClassName("active")[0];
}

function slide(event) {
    let activeCard = cards[counter];
    activeCard.classList.remove("active");
    activeCard.classList.add("gone");
    if (counter==cards.length-1) { // get back all cards
        // for (let i=0; i<cards.length; i++) {
        //     cards[i].classList.remove("gone");
        //     // rotate them back
        //     let rotation=getComputedStyle(cards[i]).getPropertyValue('--rotation');
        //     cards[i].style.transform="rotate(" + rotation + ")";
        // }
        // cards[0].classList.add("active");
        // counter=0;
        document.getElementById("message").style.display="block";
        document.getElementById("container").style.display="none";
        document.getElementById("messageh1").innerHTML+=`${correctCounter} points out of 7`;
    }
    else {
        cards[counter+1].classList.add("active");
        cards[counter+1].style.transform="rotate(0deg)";
        counter++;
    }
}

const delay = ms => new Promise(res => setTimeout(res, ms));

async function correct(nr, event) {
    freeze=true;
    document.getElementById("containerAnswers").children[nr-1].classList.add("correct");
    await delay(1000);
    slide(event);
    updateAnswers();
    freeze=false;
}

function wrong(nr) {
    document.getElementById("containerAnswers").children[nr-1].classList.add("wrong");
    gotWrong=true;
}

function handleAnswer(nr, event) {
    if (freeze) return;
    if (answers[counter]==nr-1) {
        if (!gotWrong) {
            correctCounter++;
        }
        correct(nr);
    } else {
        wrong(nr);
    }
}

function updateAnswers () {
    let containerAnswers=document.getElementById("containerAnswers");
    for (let i=0; i< 4; i++) {
        containerAnswers.children[i].innerHTML=answerPossibilities[counter][i];
        containerAnswers.children[i].classList.remove("wrong");
        containerAnswers.children[i].classList.remove("correct");
    }
    gotWrong=false;
}